package sph.durga.rCard.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.linkedin.platform.LISession;
import com.linkedin.platform.LISessionManager;
import com.linkedin.platform.errors.LIAuthError;
import com.linkedin.platform.listeners.AuthListener;
import com.linkedin.platform.utils.Scope;

import sph.durga.rCard.BaseActivity;
import sph.durga.rCard.HomeScreenActivity;
import sph.durga.rCard.R;

/**
 * Created by durga on 4/10/15.
 */
public class LoginScreenActivity extends BaseActivity
{
    static LISessionManager liManager;
    private Context mContext;
    @Override
    protected void onCreate(Bundle arg0)
    {
        super.onCreate(arg0);
        setContentView(R.layout.login_screen);
        liManager = LISessionManager.getInstance(getApplicationContext());
        mContext = this;
    }
    final Activity activity = this;
    AuthListener authListener = new AuthListener()
    {
        @Override
        public void onAuthSuccess() {
            LISession session = liManager.getSession();
            String token = session.getAccessToken().getValue();
            Intent in = new Intent(mContext, HomeScreenActivity.class);
            startActivity(in);
            //GetBasicProfile();
        }
        @Override
        public void onAuthError(LIAuthError error) {

        }
    };

    public void LinkedinSignIn(View view)
    {
        liManager.getInstance(getApplicationContext()).init(activity, buildScope(),authListener, true);
    }

    private static Scope buildScope()
    {
        return Scope.build(Scope.R_FULLPROFILE, Scope.R_EMAILADDRESS);
    }


    static int req;
    static int res;
    static Intent d;
    @Override
    protected void  onActivityResult(int requestCode, int resultCode, Intent data)
    {
        liManager.onActivityResult(this, requestCode, resultCode, data);
    }
//        if (requestCode == LISessionManager.LI_SDK_AUTH_REQUEST_CODE) {
//            // got result
//            if (resultCode == Activity.RESULT_OK) {
//                String token = data.getStringExtra(AUTH_TOKEN);
//                long expiresOn = data.getLongExtra("expiresOn", 0L);
//                AccessToken accessToken = new AccessToken(token, expiresOn);
//                liManager.init(accessToken);
//
//            } else if (resultCode == Activity.RESULT_CANCELED) {
//                authListener.onAuthError(new LIAuthError(LIAppErrorCode.USER_CANCELLED, "user canceled"));
//            } else {
//                String errorInfo = data.getStringExtra(LI_ERROR_INFO);
//                String errorDesc = data.getStringExtra(LI_ERROR_DESCRIPTION);
//                authListener.onAuthError(new LIAuthError(errorInfo, errorDesc));
//            }
//            authListener = null;
//    }
}

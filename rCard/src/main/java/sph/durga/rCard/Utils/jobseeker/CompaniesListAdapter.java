//package sph.durga.rCard.Utils.jobseeker;
//
//import android.app.Activity;
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.widget.CardView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.CheckBox;
//import android.widget.TextView;
//
//import java.util.ArrayList;
//
//import sph.durga.rCard.R;
//import sph.durga.rCard.jobseeker.SaveCompanyInfoActivity;
//
//public class CompaniesListAdapter extends BaseAdapter
//{
//	private Context mContext;
//	ArrayList<CompanyDisplay> companiesList;
//
//	public CompaniesListAdapter(Context c, ArrayList<CompanyDisplay> compList) {
//		mContext = c;
//		companiesList = compList;
//	}
//
//	public int getCount() {
//		return companiesList.size();
//	}
//
//	public Object getItem(int position) {
//		return null;
//	}
//
//	public long getItemId(int position) {
//		return 0;
//	}
//
//	// create a new ImageView for each item referenced by the Adapter
//	public View getView(int position, View convertView, ViewGroup parent)
//	{
//		View rowView;
//		CompanyDisplay displayObj;
//		CardView card;
//		final String name;
//		final String link;
//		if (convertView == null)
//		{
//			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
//			rowView = inflater.inflate(R.layout.jobseeker_company_row_grid, parent, false);
//			displayObj = companiesList.get(position);
//			card = (CardView) rowView.findViewById(R.id.cardView);
//			TextView companyName = (TextView) rowView.findViewById(R.id.companyName);
//			CheckBox RcardSentCBox = (CheckBox) rowView.findViewById(R.id.RcardSentCBox);
//			name = displayObj.getName();
//			link = displayObj.getLink();
//			companyName.setText(name);
//			card.setTag(new String[] {name, link});
//			boolean isrCardSent = displayObj.isRcardSent();
//			if(isrCardSent)
//			{
//				RcardSentCBox.setText(R.string.yes);
//			}
//			else
//			{
//				RcardSentCBox.setText(R.string.no);
//			}
//			RcardSentCBox.setChecked(isrCardSent);
//			card.setOnClickListener(new OnClickListener() {
//
//				@Override
//				public void onClick(View v)
//				{
//					Intent in = new Intent(mContext, SaveCompanyInfoActivity.class);
//					String[] tag = (String[]) v.getTag();
//					in.putExtra(sph.durga.rCard.Constants.editcompany, tag);
//					mContext.startActivity(in);
//				}
//			});
//			//editButton.setTag(new String[] {name, link});
//			//editButton.setOnClickListener(new OnClickListener() {
//			//				@Override
//			//				public void onClick(View v)
//			//				{
//			//					Intent in = new Intent(mContext, SaveCompanyInfoActivity.class);
//			//					String[] tag = (String[]) ((Button)v).getTag();
//			//					in.putExtra(sph.durga.rCard.Constants.editcompany, tag);
//			//					mContext.startActivity(in);
//			//				}
//			//			});
//		}
//		else
//		{
//			rowView = convertView;
//		}
//		return rowView;
//	}
//}

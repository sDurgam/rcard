//package sph.durga.rCard.Utils.jobseeker;
//
//import android.content.Context;
//import android.content.Intent;
//import android.support.v7.widget.CardView;
//import android.support.v7.widget.RecyclerView;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.CheckBox;
//import android.widget.TextView;
//
//import java.util.Collections;
//import java.util.List;
//
//import sph.durga.rCard.R;
//import sph.durga.rCard.Utils.jobseeker.CompanyRecyclerAdapter.CompaniesView;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Company;
//import sph.durga.rCard.jobseeker.SaveCompanyInfoActivity;
//
//public class CompanyRecyclerAdapter extends RecyclerView.Adapter<CompaniesView>
//{
//    Context ctx;
//	List<CompanyDisplay> data = Collections.emptyList();
//	LayoutInflater inflater;
//	public CompanyRecyclerAdapter(Context ctx, List<CompanyDisplay> data)
//	{
//		super();
//		inflater = LayoutInflater.from(ctx);
//        this.ctx = ctx;
//		this.data = data;
//	}
//
//	@Override
//	public int getItemCount()
//	{
//		return data.size();
//	}
//
//	@Override
//	public void onBindViewHolder(CompaniesView holder, int pos)
//	{
//        CompanyDisplay cmp =data.get(pos);
//		holder.name.setText(cmp.getName());
//		holder.issent.setChecked(cmp.rcardSent);
//        holder.card.setTag(new String[]{cmp.getName(), cmp.getLink()});
//	}
//
//	@Override
//	public CompaniesView onCreateViewHolder(ViewGroup view, int arg1)
//	{
//
//		View v = inflater.inflate(sph.durga.rCard.R.layout.jobseeker_company_row_grid, view, false);
//		return new CompaniesView(v);
//	}
//
//
//	class CompaniesView extends RecyclerView.ViewHolder
//	{
//        CardView card;
//		TextView name;
//		CheckBox issent;
//		public CompaniesView(View view)
//		{
//			super(view);
//			name = (TextView) view.findViewById(sph.durga.rCard.R.id.companyName);
//			issent = (CheckBox) view.findViewById(R.id.RcardSentCBox);
//            card = (CardView) view.findViewById(R.id.cardView);
//            card.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v)
//                {
//                       Intent in = new Intent(ctx, SaveCompanyInfoActivity.class);
//                            String[] tag = (String[]) v.getTag();
//                            in.putExtra(sph.durga.rCard.Constants.editcompany, tag);
//                            ctx.startActivity(in);
//                }
//            });
//		}
//	}
//
//}
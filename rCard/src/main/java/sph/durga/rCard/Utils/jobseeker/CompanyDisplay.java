package sph.durga.rCard.Utils.jobseeker;

public class CompanyDisplay 
{

	String name;
	String link;
	public void setLink(String link) {
		this.link = link;
	}
	public String getLink() {
		return link;
	}
	public String getName() {
		return name;
	}
	public void setName(String companyName) {
		this.name = companyName;
	}
	public boolean isRcardSent() {
		return rcardSent;
	}
	public void setRcardSent(boolean rcardSent) {
		this.rcardSent = rcardSent;
	}
	boolean rcardSent;
}

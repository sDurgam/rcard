package sph.durga.rCard.Utils;

import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;

import sph.durga.rCard.BaseActivity;

/**
 * Created by durga on 4/13/15.
 */
public class rCardActivity extends BaseActivity
{
    protected EditText emailTxt;
    protected EditText nameTxt;
    protected EditText phoneTxt;
    protected EditText countryTxt;
    protected EditText locationnameTxt;
    protected EditText industryTxt;
    protected  Spinner expYearsVal;
    protected EditText curPositionTxt;
    protected EditText highestDegreeTxt;
    protected  GridView skillsGrid;
    protected  GridView PortfolioGrid;
    protected EditText linkedinTxt;
    protected EditText resumeTxt;
    protected EditText otherinfoTxt;
}

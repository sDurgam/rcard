package sph.durga.rCard;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.cengalabs.flatui.FlatUI;

import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;

public class BaseActivity extends ActionBarActivity {

	protected SQLiteDBHelper dbHelper;
    protected Toolbar toolbar;

	@Override
	protected void onCreate(Bundle arg0)
	{
		super.onCreate(arg0);
        FlatUI.initDefaultValues(this);
        FlatUI.setDefaultTheme(FlatUI.SEA);
	}

    protected void SetUpToolBar()
    {
        toolbar.setTitle(null);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            NavUtils.navigateUpFromSameTask(this);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
	protected void onPause() {
		dbHelper.close();
		super.onPause();
	}

	@Override
	protected void onResume() {
		super.onResume();
		dbHelper = new SQLiteDBHelper(this);
	}

	@Override
	public void onBackPressed()
	{
		super.onBackPressed();
	}

    public void FooterrCardsClicked(View view)
    {

    }

    public void FooterhistoryClicked(View view)
    {

    }

    public void FooterrcollaboratorsClicked(View view)
    {

    }

    public void FooterprofileClicked(View view)
    {

    }

}

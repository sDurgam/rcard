package sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;

/**
 * Created by durga on 4/15/15.
 */
public class ProfileField
{
    int id;
    String name;

    protected String getName()
    {
        return name;
    }

    protected void setName(String name)
    {
        this.name = name;
    }

    protected ProfileField()
    {
        super();
    }
    protected ProfileField(String name)
    {
        this.name = name;
    }

    protected ProfileField(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    protected int InsertField(SQLiteDatabase writer, String name)
    {
        int id = -1;
        String query = "SELECT MAX(%s) + 1 FROM %s";
        String getprofilefieldsquery = "select %s from %s";
        Cursor cursor = writer.rawQuery(String.format(query, SQLiteDBHelper.FIELDS_ID, SQLiteDBHelper.TABLE_FIELDS), null);
        cursor.moveToFirst();
        if(cursor.getCount() != 0)
        {
            id = cursor.getInt(0);
            cursor.close();
            if(id == 0)
                id = 1;
            ContentValues cv = new ContentValues();
            cv.put(SQLiteDBHelper.FIELDS_ID, id);
            cv.put(SQLiteDBHelper.FIELDS_NAME, name);
            writer.insert(SQLiteDBHelper.TABLE_FIELDS, null, cv);
            return id;
//            //update that to the profile fields
//            cursor = writer.rawQuery(String.format(getprofilefieldsquery, SQLiteDBHelper.FIELDS_ID, SQLiteDBHelper.TABLE_PROFILE), null);
//            cursor.moveToFirst();
//            String fieldsStr = String.valueOf(cursor.getInt(0));
//            cursor.close();
//            //add new field value to profile fields
//            fieldsStr = fieldsStr + Constants.delimiter + id;

        }
        return id;
    }

    protected void DeleteField(SQLiteDatabase writer, String name)
    {
        String whereClause = SQLiteDBHelper.FIELDS_NAME + "?";
        String[] whereArgs = {name};
        writer.delete(SQLiteDBHelper.TABLE_FIELDS, whereClause, whereArgs);
    }
}

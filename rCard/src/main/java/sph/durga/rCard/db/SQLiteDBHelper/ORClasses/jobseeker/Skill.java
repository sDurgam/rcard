package sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;

/**
 * Created by durga on 4/13/15.
 */
public class Skill
{

    int id;
    String name;
    String exp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExp() {
        return exp;
    }

    public void setExp(String exp) {
        this.exp = exp;
    }

    public Skill()
    {

    }

    public Skill(int id, String name, String exp)
    {
        this.id = id;
        this.name = name;
        this.exp = exp;
    }

    public String makePlaceholders(int len)
    {
        String str = null;
        if(len >= 1)
        {
            StringBuilder sb = new StringBuilder(len * 2 - 1);
            sb.append("?");
            for (int i = 1; i < len; i++)
            {
                sb.append(",?");
            }
            str = sb.toString();
        }
        return str;
    }

    protected ArrayList<Skill> GetSkillsList(SQLiteDatabase reader, String skillsIdStr)
    {
        ArrayList<Skill> skillsList = new ArrayList<Skill>();
        String[] skillsIds = skillsIdStr.split(",");
        String skills = makePlaceholders(skillsIdStr.length());
      //  SQLiteDatabase db = dbHelper.getWritableDatabase();
        String query = "SELECT * FROM " + SQLiteDBHelper.TABLE_SKILLS
                + " WHERE " + SQLiteDBHelper.SKILLS_ID + " IN (" + makePlaceholders(skillsIds.length) + ")";
        Cursor cursor = reader.rawQuery(query, skillsIds);
        if(cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            do
            {
                skillsList.add(new Skill(cursor.getInt(0), cursor.getString(1), cursor.getString(2)));
            }while(cursor.moveToNext());
        }
        cursor.close();;
        return  skillsList;
    }

    protected void AddSkill(SQLiteDatabase writer, ArrayList<Skill> skillsList)
    {
        for(int i =0; i < skillsList.size(); i++)
        {
            AddSkill(writer, skillsList.get(i));
        }
    }

    private void AddSkill(SQLiteDatabase writer, Skill skill)
    {
        String query = "SELECT max(%s) FROM %s";
        Cursor cursor = writer.rawQuery(String.format(query, SQLiteDBHelper.SKILLS_ID, SQLiteDBHelper.TABLE_SKILLS), null);
        int id = 1;
        if(cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            id = cursor.getInt(0);
            if(id == 0)
            {
                id = 1;
            }
        }
        cursor.close();
        ContentValues cv = new ContentValues();
        cv.put(SQLiteDBHelper.SKILLS_ID, skill.id);
        cv.put(SQLiteDBHelper.SKILLS_NAME, skill.name);
        cv.put(SQLiteDBHelper.SKILLS_EXP, skill.exp);
        writer.insert(SQLiteDBHelper.SKILLS, null, cv);
    }

}

//package sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker;
//
//import android.database.Cursor;
//import android.database.sqlite.SQLiteDatabase;
//
//import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;
//
//public class Company
//{
//	int id;
//	String name;
//	String link;
//	String contact;
//	String email;
//	String othinfo;
//	boolean isrCardSent;
//	public int getId() {
//		return id;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public String getLink() {
//		return link;
//	}
//
//	public String getContact() {
//		return contact;
//	}
//
//	public String getEmail() {
//		return email;
//	}
//
//	public String getOthinfo() {
//		return othinfo;
//	}
//
//	public boolean isIsrCardSent() {
//		return isrCardSent;
//	}
//
//	public Company(String name, String link)
//	{
//		this.name = name;
//		this.link = link;
//		this.contact = null;
//		this.email = null;
//		this.contact = null;
//		this.othinfo = null;
//		this.isrCardSent = false;
//	}
//
//	public void GetCompany(SQLiteDBHelper db)
//	{
//		SQLiteDatabase writer = db.getWritableDatabase();
//		String whereClause = SQLiteDBHelper.COMPANY_NAME + "= ? AND " + SQLiteDBHelper.COMPANY_URL + "= ?";
//		String[] whereArgs = { name, link };
//		Cursor cursor = writer.query(SQLiteDBHelper.TABLE_COMPANY, new String[] { SQLiteDBHelper.COMPANY_NAME, SQLiteDBHelper.COMPANY_URL, SQLiteDBHelper.COMPANY_CONTACT_NAME, SQLiteDBHelper.COMPANY_EMAIL, SQLiteDBHelper.COMPANY_OTHER_INFO, SQLiteDBHelper.COMPANY_MYRCARD_SENT}, whereClause, whereArgs, null, null, null);
//		if(cursor.getCount() > 0)
//		{
//			cursor.moveToFirst();
//			name = cursor.getString(0);
//			link = cursor.getString(1);
//			contact = cursor.getString(2);
//			email = cursor.getString(3);
//			othinfo = cursor.getString(4);
//			isrCardSent = (cursor.getInt(5) > 0 ? true : false);
//			cursor.close();
//			//update
//		}
//		writer.close();
//	}
//}

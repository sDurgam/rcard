package sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import sph.durga.rCard.Constants;
import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;

/**
 * Created by durga on 4/13/15.
 */
public class MyProfile
{
    SQLiteDBHelper dbHelper;

    public Hashtable<String, Object> getProfileFieldValuesMap() {
        return profileFieldValuesMap;
    }

    Hashtable<String, Object> profileFieldValuesMap;
    ProfileField pfieldsObj;
    Skill skillObj;

//    String email = "";
//    String name = "";
//    String phone = "";
//    String country = "";
//    String locationname = "";
//
//    String industry = "";
//    Integer expYearsVal = 0;
//    String curPosition = "";
//    String highestDegree = "";
//    String school = "";
//
//    ArrayList<Skill> skills;
//    ArrayList<String> portfolios;
//
//    String linkedinurl = "";
//    String resumeurl = "";
//    String otherinfo = "";

    public ArrayList<String> getFieldsList()
    {
        return (ArrayList<String>)(profileFieldValuesMap.keys());
    }

    public void setFieldsList(ArrayList<String> fieldsList)
    {
        for(int i =0; i < fieldsList.size(); i++) {
            profileFieldValuesMap.put(fieldsList.get(i), "");
        }
    }

//    public void AddField(String field, String value)
//    {
//        profileFieldValuesMap.put(field, value);
//    }

//    public SQLiteDBHelper getDbHelper() {
//		return dbHelper;
//	}
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getPhone() {
//        return phone;
//    }
//
//    public void setPhone(String phone) {
//        this.phone = phone;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getCountry() {
//        return country;
//    }
//
//    public void setCountry(String country) {
//        this.country = country;
//    }
//
//    public String getLocationname() {
//        return locationname;
//    }
//
//    public void setLocationname(String locationname) {
//        this.locationname = locationname;
//    }
//
//    public String getIndustry() {
//        return industry;
//    }
//
//    public void setIndustry(String industry) {
//        this.industry = industry;
//    }
//
//    public Integer getExpYearsVal() {
//        return expYearsVal;
//    }
//
//    public void setExpYearsVal(Integer expYearsVal) {
//        this.expYearsVal = expYearsVal;
//    }
//
//    public String getCurPositionTxt() {
//        return curPosition;
//    }
//
//    public void setCurPositionTxt(String curPositionTxt) {
//        this.curPosition = curPositionTxt;
//    }
//
//    public String getHighestDegreeTxt() {
//        return highestDegree;
//    }
//
//    public void setHighestDegreeTxt(String highestDegreeTxt) {
//        this.highestDegree = highestDegreeTxt;
//    }
//
//    public String getSchool() {
//        return school;
//    }
//
//    public void setSchool(String school) {
//        this.school = school;
//    }
//
//
//    public ArrayList<Skill> getSkills() {
//        return skills;
//    }
//
//    public void setSkills(ArrayList<Skill> skills) {
//        this.skills = skills;
//    }
//
//    public ArrayList<String> getPortfolios() {
//        return portfolios;
//    }
//
//    public void setPortfolios(ArrayList<String> portfolios) {
//        this.portfolios = portfolios;
//    }
//
//    public String getLinkedinurl() {
//        return linkedinurl;
//    }
//
//    public void setLinkedinurl(String linkedinurl) {
//        this.linkedinurl = linkedinurl;
//    }
//
//    public String getResumeurl() {
//        return resumeurl;
//    }
//
//    public void setResumeurl(String resumeurl) {
//        this.resumeurl = resumeurl;
//    }
//
//    public String getOtherinfo() {
//        return otherinfo;
//    }
//
//    public void setOtherinfo(String otherinfo) {
//        this.otherinfo = otherinfo;
//    }

    public MyProfile(SQLiteDBHelper dbhelper)
    {
        super();
        dbHelper = dbhelper;
        pfieldsObj = new ProfileField();
        profileFieldValuesMap = new Hashtable<String, Object>();
    }

    public void FetchMyProfile()
    {
        SQLiteDatabase reader = dbHelper.getReadableDatabase();
        String skillsStr;
        String portfoliosStr;
        Cursor cursor;
        //String[] fieldsArray = new String[getFieldsList().size()];
        //getFieldsList().toArray(fieldsArray);
        String name;
        Object value = null;
        //cursor = reader.query(SQLiteDBHelper.TABLE_PROFILE, new String[] { SQLiteDBHelper.PROFILE_EMAIL, SQLiteDBHelper.PROFILE_NAME, SQLiteDBHelper.PROFILE_PHONE, SQLiteDBHelper.PROFILE_COUNTRY, SQLiteDBHelper.PROFILE_LOCATION_NAME, SQLiteDBHelper.PROFILE_INDUSTRY, SQLiteDBHelper.PROFILE_TOTAL_EXP_YEARS_VAL, SQLiteDBHelper.PROFILE_CUR_POSITION, SQLiteDBHelper.PROFILE_HIGHEST_DEGREE,  SQLiteDBHelper.PROFILE_SCHOOL, SQLiteDBHelper.PROFILE_SKILLS, SQLiteDBHelper.PROFILE_PORTFOLIOS, SQLiteDBHelper.PROFILE_LINKEDIN_URL, SQLiteDBHelper.PROFILE_RESUME_URL, SQLiteDBHelper.PROFILE_OTHER_INFO},
        //		                                                         null, null, null, null, null);
        final String MY_QUERY = "SELECT b.%s, a.%s FROM %s a INNER JOIN %s b ON a.%s=b.%s";

        cursor = reader.rawQuery(String.format(MY_QUERY, SQLiteDBHelper.FIELDS_NAME, SQLiteDBHelper.PROFILE_FIELD_VALUE, SQLiteDBHelper.TABLE_PROFILE, SQLiteDBHelper.TABLE_FIELDS, SQLiteDBHelper.PROFILE_FIELD_ID, SQLiteDBHelper.FIELDS_ID), null);
        //cursor = reader.query(SQLiteDBHelper.TABLE_PROFILE, new String[]{SQLiteDBHelper.PROFILE_FIELD_ID, SQLiteDBHelper.PROFILE_FIELD_VALUE}, null, null, null, null, null,null);
        if(cursor.getCount() > 0)
        {
            cursor.moveToFirst();
            do {
                name = cursor.getString(0);
                if(name.equals(SQLiteDBHelper.SKILLS))
                {
                    String skillIds = cursor.getString(1);
                    value = ParseSkills(skillIds);
                }
                else if(name.equals(SQLiteDBHelper.PORTFOLIOS))
                {
                    value = ParsePortfolios(cursor.getString(1));
                }
                else
                {
                    value = cursor.getString(1);
                }
                profileFieldValuesMap.put(name, value);
            }while(cursor.moveToNext());
        }
        cursor.close();
        reader.close();
    }


    public void SaveLinkedinProfile(Hashtable<String, Object> data)
    {
        profileFieldValuesMap = data;
        saveProfile();
    }

    private long saveProfile()
    {
        long result = -1;

        ContentValues cv = new ContentValues();
        SQLiteDatabase writer = dbHelper.getWritableDatabase();
        //delete any existing profile
        writer.delete(SQLiteDBHelper.TABLE_PROFILE, null, null);

        Iterator it = profileFieldValuesMap.keySet().iterator();
        //Map.Entry<String, Object> fieldValue;
        String key = "";
        String value = "";
        while(it.hasNext())
        {
            //fieldValue = (Map.Entry<String, Object>) it.next();
            key = it.next().toString();
            //if skills get skill ids
            if(key.equals(SQLiteDBHelper.SKILLS))
            {
                //Insert skills into skills table
                if(((ArrayList<Skill>)profileFieldValuesMap.get(key)).size() > 0)
                {
                    Skill skillObj = new Skill();
                    skillObj.AddSkill(writer, (ArrayList<Skill>)profileFieldValuesMap.get(key));
                    value = GetSkillIdsString(((ArrayList<Skill>) profileFieldValuesMap.get(key)));
                }
                else
                {
                    value = "";
                }
            }
            //if portfolios split by ","
            else if(key.equals(SQLiteDBHelper.PORTFOLIOS))
            {
                if(profileFieldValuesMap.get(key).toString().contains(",")) {
                    value = GetDelimitedStrings(((ArrayList<String>) profileFieldValuesMap.get(key)));
                }
                else
                {
                    value = "";
                }
            }
            else
            {
                value = profileFieldValuesMap.get(key).toString();
            }
            int fieldId = -1;
            String whereClause = SQLiteDBHelper.FIELDS_NAME + "= ?";
            //Get Field Id from Fields table
            Cursor fieldIdcursor = writer.query(SQLiteDBHelper.TABLE_FIELDS, new String[] {SQLiteDBHelper.FIELDS_ID}, whereClause , new String[] {key}, null, null, null);
            if(fieldIdcursor.getCount() > 0)
            {
                fieldIdcursor.moveToFirst();
                fieldId = fieldIdcursor.getInt(0);
                fieldIdcursor.close();;
                cv.put(SQLiteDBHelper.PROFILE_FIELD_ID, fieldId);
                cv.put(SQLiteDBHelper.PROFILE_FIELD_VALUE, value);
                result = writer.insert(SQLiteDBHelper.TABLE_PROFILE, null, cv);
            }
            else {
                fieldIdcursor.close();
                Log.e(Constants.TAG, "Saving linkedin profile failed as field value could not be found");
            }
        }
        writer.close();
        return result;
    }

    //insert in profiles table and also fields table
    public void InsertNewField(String name, String value)
    {
        SQLiteDatabase writer = dbHelper.getWritableDatabase();
        int id = pfieldsObj.InsertField(writer, name);
        if(id == -1)
        {
            Log.e(Constants.TAG, "Failed to add new field");
        }
        else {
            ContentValues cv = new ContentValues();
            cv.put(SQLiteDBHelper.PROFILE_FIELD_ID, name);
            cv.put(SQLiteDBHelper.PROFILE_FIELD_VALUE, value);
            writer.insert(SQLiteDBHelper.TABLE_PROFILE, null, cv);
        }
        writer.close();
    }

    //update fields value in the profile table
    public void UpdateProfileFieldValue(String key, String value)
    {
        SQLiteDatabase writer = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(key, value);
        writer.update(SQLiteDBHelper.TABLE_PROFILE, cv, null, null);
        writer.close();
    }

    //delete field in both profile and fields tables
    public void DeleteProfileField(String name)
    {
        pfieldsObj.DeleteField(dbHelper.getWritableDatabase(), name);
    }

    private String GetSkillIdsString(ArrayList<Skill> skills)
    {
        StringBuilder skillIdsBuilder = new StringBuilder();
        ArrayList<String> skillIdsStr = new ArrayList<String>();
        int length = skills.size();
        for(int i =0; i < length - 1; i++)
        {
            skillIdsBuilder.append(skills.get(i).getId());
            skillIdsBuilder.append(Constants.delimiter);
        }
        skillIdsBuilder.append(skills.get(length - 1).getId());
        return skillIdsBuilder.toString();
    }

    private String GetDelimitedStrings(ArrayList<String> Str)
    {
        StringBuilder strBuilder = new StringBuilder();
        int length = Str.size();
        for(int i =0; i < length - 1; i++)
        {
            strBuilder.append(Str.get(i));
            strBuilder.append(Constants.delimiter);
        }
        strBuilder.append(Str.get(length - 1));
        return strBuilder.toString();
    }

    private ArrayList<Skill> ParseSkills(String skillIds)
    {
        SQLiteDatabase reader = dbHelper.getWritableDatabase();
        ArrayList<Skill> skills = new ArrayList<Skill>();
        Skill skillsObj = new Skill();
        skills = skillsObj.GetSkillsList(reader, skillIds);
        reader.close();
        return skills;
    }

    private ArrayList<String> ParsePortfolios(String portfoliosStr)
    {
        ArrayList<String> portfolios = new ArrayList<String>();
        String[] str = portfoliosStr.split(",");
        for(int i =0; i < str.length; i++)
        {
            portfolios.add(str[i]);
        }
        return portfolios;
    }
}

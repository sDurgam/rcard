//package sph.durga.rCard.jobseeker;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.widget.TextView;
//import java.util.ArrayList;
//import sph.durga.rCard.BaseActivity;
//import sph.durga.rCard.R;
//import sph.durga.rCard.Utils.jobseeker.CompanyDisplay;
//import sph.durga.rCard.Utils.jobseeker.CompanyRecyclerAdapter;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Companies;
//
//public class DisplayComapaniesActivity extends BaseActivity
//{
//	Companies companiesObj;
//	RecyclerView recyclerView;
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState)
//	{
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.jobseeker_list_company_activity);
//		recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        SetUpToolBar();
//	}
//
//	@Override
//	protected void onResume()
//	{
//		super.onResume();
//		companiesObj = new Companies(dbHelper);
//		ArrayList<CompanyDisplay> companiesList = companiesObj.GetcompaniesList();
//		recyclerView.setLayoutManager(new LinearLayoutManager(getParent()));
//		recyclerView.setHasFixedSize(true);
//		recyclerView.setAdapter(new CompanyRecyclerAdapter(this, companiesList));
//	}
//
//	public void CardViewClick(View view)
//	{
//		TextView companyTxtView = (TextView)view.findViewById(R.id.companyName);
//	}
//
//	public void addCompanyBtn(View view)
//	{
//		Intent in = new Intent(this, SaveCompanyInfoActivity.class);
//		startActivity(in);
//	}
//}

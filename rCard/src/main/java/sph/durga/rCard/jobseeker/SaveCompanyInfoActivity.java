//package sph.durga.rCard.jobseeker;
//
//import android.app.Activity;
//import android.bluetooth.BluetoothAdapter;
//import android.bluetooth.BluetoothDevice;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.os.Message;
//import android.text.Editable;
//import android.text.TextWatcher;
//import android.view.View;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import sph.durga.rCard.BaseActivity;
//import sph.durga.rCard.Constants;
//import sph.durga.rCard.R;
//import sph.durga.rCard.bluetooth.rCardService;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Companies;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Company;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyRcard;
//import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;
//
//public class SaveCompanyInfoActivity extends BaseActivity implements TextWatcher
//{
//	EditText companynameTxt;
//	EditText companylinkTxt;
//	EditText companycontactTxt;
//	EditText companyemailTxt;
//	EditText companyotherInfoTxt;
//	TextView msgTxt;
//	Button saverCardBtn;
//	Button sendrCardBtn;
//    Button nfcBtn;
//	int companyId = -1;
//	private MyRcard myrcardObj;
//	private static Context mContext;
//
//	private static final int REQUEST_CONNECT_DEVICE = 2;
//	private static final int REQUEST_ENABLE_BT = 1;
//	String[] company = new String[2];
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState)
//	{
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.jobseeker_save_company_activity);
//		companynameTxt = (EditText) findViewById(R.id.companynameTxt);
//		companylinkTxt = (EditText) findViewById(R.id.companylinkTxt);
//		companycontactTxt = (EditText) findViewById(R.id.companycontactTxt);
//		companyemailTxt = (EditText) findViewById(R.id.companyemailTxt);
//		companyotherInfoTxt = (EditText) findViewById(R.id.companyotherInfoTxt);
//		saverCardBtn = (Button) findViewById(R.id.saverCardBtn);
//		sendrCardBtn = (Button) findViewById(R.id.sendrCardBtn);
//        nfcBtn = (Button) findViewById(R.id.sendrCardBtnNFC);
//		msgTxt = (TextView) findViewById(R.id.msgTxt);
//		mContext = this;
//		//required field validations
//		companynameTxt.addTextChangedListener(this);
//		companylinkTxt.addTextChangedListener(this);
//		companycontactTxt.addTextChangedListener(this);
//		companyemailTxt.addTextChangedListener(this);
//		companyotherInfoTxt.addTextChangedListener(this);
//
////		//if string[] edit company is not null then display company for edit mode
//	}
//
//	private void DisplayCompanyInfo(String name, String link)
//	{
//		Company cmpObj = new Company(name, link);
//		cmpObj.GetCompany(dbHelper);
//		if(cmpObj.getContact() != null)
//		{
//			companynameTxt.setText(name);
//			companylinkTxt.setText(link);
//			companycontactTxt.setText(cmpObj.getContact());
//			companyotherInfoTxt.setText(cmpObj.getOthinfo());
//			companyemailTxt.setText(cmpObj.getEmail());
//		}
//	}
//
//	private int SaveCompanyInfo()
//	{
//		String companyname = companynameTxt.getText().toString();
//		String companylink = companylinkTxt.getText().toString();
//		String contactname = companycontactTxt.getText().toString();
//		String email = companyemailTxt.getText().toString();
//		String otherinfo = companyotherInfoTxt.getText().toString();
//		int result = -1;
//
//		if(!companyname.equals(""))
//		{
//			Companies cmpObj = new Companies(dbHelper);
//			result = cmpObj .SaveCompany(companyname, companylink, contactname, email, otherinfo);
//			if(result == -1)
//			{
//				msgTxt.setText("company details could not be saved");
//			}
//			else
//			{
//				msgTxt.setText("company details successfully saved");
//			}
//		}
//		else
//		{
//			msgTxt.setText("please enter company name");
//		}
//		return result;
//	}
//
//	@Override
//	protected void onPause()
//	{
//		super.onPause();
//		if(rCardService != null)
//		{
//			rCardService.stop();
//		}
//		dbHelper = null;
//	}
//
//	@Override
//	protected void onDestroy() {
//		if(rCardService != null)
//		{
//			rCardService.stop();
//		}
//		super.onDestroy();
//	}
//
//	@Override
//	protected void onResume()
//	{
//		super.onResume();
//		if(this.getIntent().getExtras() != null && this.getIntent().getExtras().get(Constants.editcompany) != null)
//		{
//			String[] company = (String[]) this.getIntent().getExtras().get(Constants.editcompany);
//			//Get company details and display them
//			DisplayCompanyInfo(company[0], company[1]);
//		}
//	}
//
//	public void saveCompanyInfoClick(View view)
//	{
//		companyId = SaveCompanyInfo();
//	}
//
//	public void sendRcardBTClick(View view)
//	{
//		if(companyId == -1)
//		{
//			companyId = SaveCompanyInfo();
//            SetupBluetooth();
//		}
//		else
//		{
//			SetupBluetooth();
//		}
//	}
//
//    public void sendRcardNFCClick(View view)
//    {
//        Intent nfcActivity = new Intent(this, SendViaNFCActivity.class);
//        startActivity(nfcActivity);
//    }
//
//	BluetoothAdapter btAdapter;
//	rCardService rCardService;
//
//	private void SetupBluetooth()
//	{
//		btAdapter = BluetoothAdapter.getDefaultAdapter();
//		if (btAdapter == null)
//		{
//
//			Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_LONG).show();
//			this.finish();
//		}
//		else
//		{
//			if (!btAdapter.isEnabled())
//			{
//				Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
//				startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
//			}
//			else
//			{
//				ListBluetoothDevices();
//			}
//		}
//	}
//	private void ListBluetoothDevices()
//	{
//		if (rCardService == null)
//		{
//			rCardService = new rCardService(this, mHandler, Constants.sockettype.client);
//		}
//		Intent serverIntent = new Intent(this, ListDevicesActivity.class);
//		startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
//
//	}
//
//	public void onActivityResult(int requestCode, int resultCode, Intent data) {
//		switch (requestCode)
//		{
//		case REQUEST_CONNECT_DEVICE:
//			// When DeviceListActivity returns with a device to connect
//			if (resultCode == Activity.RESULT_OK)
//			{
//				connectDevice(data);
//			}
//			break;
//		case REQUEST_ENABLE_BT:
//			if (resultCode == Activity.RESULT_OK)
//			{
//				ListBluetoothDevices();
//			} else
//			{
//				Toast.makeText(this, "Bluetooth is not enabled or cannot enable bluetooth",
//						Toast.LENGTH_SHORT).show();
//				this.finish();
//			}
//		}
//	}
//	private void connectDevice(Intent data)
//	{
//		String address = data.getExtras()
//				.getString(ListDevicesActivity.EXTRA_DEVICE_ADDRESS);
//		BluetoothDevice device = btAdapter.getRemoteDevice(address);
//		rCardService.connect(device, GetrCardJSONObject());
//	}
//
//	private JSONObject GetrCardJSONObject()
//	{
//        if(dbHelper == null)
//        {
//            dbHelper = new SQLiteDBHelper(this);
//        }
//		myrcardObj = new MyRcard(dbHelper);
//		myrcardObj.FetchRCard();
//		JSONObject jsonObject = new JSONObject();
//		try
//		{
//			jsonObject.put(SQLiteDBHelper.MYRCARD_NAME, myrcardObj.getName());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_PHONE, myrcardObj.getPhone());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_EMAIL, myrcardObj.getEmail());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_PRIMARY_SKILLS, myrcardObj.getPrimaryskills());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_ANDROID_EXP, myrcardObj.getAndroidexp().toString());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_IOS_EXP, myrcardObj.getIosexp().toString());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_PORTFOLIO_ANDROID, myrcardObj.getAndurl());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_PORTFOLIO_IOS, myrcardObj.getIosurl());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_PORTFOLIO_OTHER, myrcardObj.getOthurl());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_LINKEDIN_URL, myrcardObj.getLinkurl());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_RESUME_URL, myrcardObj.getResumeurl());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_HIGHEST_DEGREE, myrcardObj.getDegree());
//			jsonObject.put(SQLiteDBHelper.MYRCARD_OTHER_INFO, myrcardObj.getOtherinfo());
//		} catch (JSONException e)
//		{
//			e.printStackTrace();
//		}
//		return jsonObject;
//	}
//
//	private final Handler mHandler = new Handler()
//	{
//		@Override
//		public void handleMessage(Message msg) {
//
//			String mConnectedDeviceName;
//			switch (msg.what)
//			{
//			case Constants.MESSAGE_DEVICE_NAME:
//				// save the connected device's name
//				mConnectedDeviceName =  msg.getData().getString(Constants.DEVICE_NAME);
//				if (null != mContext) {
//					Toast.makeText(mContext, "Connected to "
//							+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();
//				}
//				break;
//			case Constants.MESSAGE_TOAST:
//				if (null != mContext) {
//					Toast.makeText(mContext, msg.getData().getString(Constants.TOAST),
//							Toast.LENGTH_SHORT).show();
//				}
//				break;
//			case Constants.MESSAGE_JSON_DATA_WRITE:
//			{
//				//update rcard sent column of the company table
//				Companies compObj = new Companies(dbHelper);
//				compObj.UpdateMyrCard(companyId, true);
//				msgTxt.setText("Your rCard succesfully sent to " + companynameTxt.getText().toString());
//			}
//			break;
//			}
//		}
//	};
//
//	@Override
//	public void beforeTextChanged(CharSequence s, int start, int count,
//			int after) {
//
//	}
//
//	@Override
//	public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//	}
//
//	@Override
//	public void afterTextChanged(Editable s)
//	{
//		int companyname = companynameTxt.getText().toString().length();
//		int companylink = companylinkTxt.getText().toString().length();
//		if(companyname == 0)
//		{
//			if(companylink == 0)
//			{
//				msgTxt.setText(R.string.requirednamelink);
//			}
//			else
//			{
//				msgTxt.setText(R.string.requiredname);
//			}
//		}
//		else if(companylink == 0)
//		{
//			msgTxt.setText(R.string.requiredlink);
//		}
//		else
//		{
//			msgTxt.setText("");
//			saverCardBtn.setEnabled(true);
//			sendrCardBtn.setEnabled(true);
//            nfcBtn.setEnabled(true);
//		}
//	}
//}

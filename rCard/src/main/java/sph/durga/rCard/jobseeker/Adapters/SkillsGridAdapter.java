package sph.durga.rCard.jobseeker.Adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sph.durga.rCard.R;
import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Skill;

/**
 * Created by durga on 4/16/15.
 */
public class SkillsGridAdapter extends BaseAdapter
{
    private Context mContext;
    ArrayList<Skill> skillList;

    public SkillsGridAdapter(Context c, ArrayList<Skill> skillsList)
    {
        mContext = c;
        skillList = skillsList;
    }

    public int getCount() {
        return skillList.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View rowView;
        Skill skillObj;
        if (convertView == null)
        {  // if it's not recycled, initialize some attributes
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            rowView = inflater.inflate(R.layout.jobseeker_profile_skills_row, parent, false);
            skillObj = skillList.get(position);
            TextView name = (TextView) rowView.findViewById(R.id.profile_skill_txt);
            TextView exp = (TextView) rowView.findViewById(R.id.profile_exp_txt);
            name.setText(skillObj.getName());
            exp.setText(skillObj.getExp());
            }
        else
        {
            rowView = convertView;
        }
        return rowView;
    }
}

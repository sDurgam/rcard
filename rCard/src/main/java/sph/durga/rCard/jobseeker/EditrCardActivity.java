//package sph.durga.rCard.jobseeker;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.content.Context;
//import android.os.Bundle;
//import android.support.v4.app.NavUtils;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.view.Window;
//import android.widget.ArrayAdapter;
//import android.widget.EditText;
//import android.widget.ImageButton;
//
//import java.util.Timer;
//import java.util.TimerTask;
//
//import sph.durga.rCard.R;
//import sph.durga.rCard.Utils.rCardActivity;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyProfile;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyRcard;
//
//public class EditrCardActivity extends rCardActivity
//{
//	MyProfile rcardObj;
//    ImageButton tbEditBtn;
//    protected Context mContext;
//
//    @Override
//	protected void onCreate(Bundle savedInstanceState)
//	{
//		super.onCreate(savedInstanceState);
//
//        // GetBasicProfile();
//  		setContentView(R.layout.jobseeker_edit_rcard_activity);
//		nameTxt = (EditText) findViewById(R.id.nameTxt);
//		phoneTxt = (EditText) findViewById(R.id.phoneTxt);
//		emailTxt = (EditText) findViewById(R.id.emailTxt);
//		linkedinTxt = (EditText) findViewById(R.id.linkedinTxt);
//		resumeTxt = (EditText) findViewById(R.id.resumeTxt);
//        highestDegreeTxt = (EditText) findViewById(R.id.highestdegreeTxt);
//		otherinfoTxt = (EditText) findViewById(R.id.otherinfoTxt);
//        toolbar = (Toolbar) findViewById(R.id.customtoolbar);
//        tbEditBtn = (ImageButton) toolbar.findViewById(R.id.edittoolBarBtn);
//        tbEditBtn.setBackgroundResource(R.drawable.ic_action_save);
//        SetUpToolBar();
//		PopulateSpinners();
//        mContext = this;
//	}
//
//    Dialog a_dialog;
//    public void EditButtonClicked(View view)
//    {
//        a_dialog = new Dialog(this);
//        a_dialog.setTitle("saving");
//        a_dialog.show();
//        SaveAll(view);
//    }
//
//	@Override
//	protected void onResume()
//	{
//		super.onResume();
//		rcardObj = new MyRcard(dbHelper);
//		rcardObj.FetchRCard();
//		populateRCard();
//        //AddTextWatchers();
//	}
//
//	@Override
//	protected void onPause()
//	{
//		rcardObj = null;
//		super.onPause();
//	}
//
//	private void PopulateSpinners()
//	{
//		Integer[] array = new Integer[50];
//		for(int i =0; i < 50; i++)
//		{
//			array[i] = i;
//		}
//		expYearsVal.setAdapter(new ArrayAdapter<Integer>(this,
//                android.R.layout.simple_spinner_item, array));
//	}
//
//	public void SaveAll(View view)
//	{
//
//		String name = nameTxt.getText().toString();
//		String phone = phoneTxt.getText().toString();
//		String email = emailTxt.getText().toString();
//		String primaryskills = primaryskillsTxt.getText().toString();
//		String linkurl = linkedinTxt.getText().toString();
//		String resumeurl = resumeTxt.getText().toString();
//		String otherinfo = otherinfoTxt.getText().toString();
//		String degree = highestDegreeTxt.getText().toString();
//        final Activity activity = this;
//		final long result = rcardObj.saveRcard(name, phone, email, primaryskills, Integer.valueOf(androidexp), Integer.valueOf(iosexp), andurl, iosurl, othurl, linkurl, resumeurl, degree, otherinfo);
//        final Timer t = new Timer();
//        t.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                a_dialog.cancel();
//                if(result != -1)
//                {
////                   Toast.makeText(mContext, "rCard saved", Toast.LENGTH_SHORT).show();
//                    NavUtils.navigateUpFromSameTask(activity);
//                }
//                else
//                {
//                    a_dialog.cancel();
// //                   Toast.makeText(mContext, "rCard could not be saved", Toast.LENGTH_LONG).show();
//                }
//            }
//        }, 200);
//
//	}
//
//
//    private void ShowPopUpDialog(Dialog m_dialog)
//    {
//        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        m_dialog.setContentView(R.layout.rcard_save_dialog);
//        m_dialog.show();
//    }
//
//	private void populateRCard()
//	{
//		nameTxt.setText(rcardObj.getName() == null ? "" : rcardObj.getName());
//		phoneTxt.setText(rcardObj.getPhone() == null ? "" : rcardObj.getPhone());
//		emailTxt.setText(rcardObj.getEmail() == null ? "" : rcardObj.getEmail());
//		primaryskillsTxt.setText(rcardObj.getPrimaryskills() == null ? "" : rcardObj.getPrimaryskills());
//		androidexpList.setSelection(rcardObj.getAndroidexp() == null ? 0 : rcardObj.getAndroidexp());
//		iosexpList.setSelection(rcardObj.getIosexp() == null ? 0 : rcardObj.getIosexp());
//		androidportfolioTxt.setText(rcardObj.getAndurl() == null ? "" : rcardObj.getAndurl());
//		iosportfolioTxt.setText(rcardObj.getIosurl() == null ? "" : rcardObj.getIosurl());
//		othportfolioTxt.setText(rcardObj.getOthurl() == null ? "" : rcardObj.getOthurl());
//		linkedinTxt.setText(rcardObj.getLinkurl() == null ? "" : rcardObj.getLinkurl());
//		resumeTxt.setText(rcardObj.getResumeurl() == null ? "" : rcardObj.getResumeurl());
//		otherinfoTxt.setText(rcardObj.getOtherinfo() == null ? "" : rcardObj.getOtherinfo());
//		degreeTxt.setText(rcardObj.getDegree() == null ? "" : rcardObj.getDegree());
//	}
//}

package sph.durga.rCard.jobseeker;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.linkedin.platform.APIHelper;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;

import org.json.JSONObject;

import sph.durga.rCard.BaseActivity;
import sph.durga.rCard.Constants;
import sph.durga.rCard.R;
import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyProfile;
import sph.durga.rCard.jobseeker.profile.DisplayProfileActivity;

//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyRcard;

public class JobSeekerHomeActivity  extends BaseActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setContentView(R.layout.jobseeker_home_activity);
        super.onCreate(savedInstanceState);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        SetUpToolBar();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        //check if rCard is null if null then build rCard
        MyProfile myprofileObj = new MyProfile(dbHelper);
        myprofileObj.FetchMyProfile();
        if(myprofileObj.getProfileFieldValuesMap().size() == 0)
        {
            //Get values from linkedin and save them
            SaveLinkedinProfileData(myprofileObj);
        }
        else
        {
           Intent in = new Intent(this, DisplayProfileActivity.class);
           startActivity(in);
        }
    }

    private void SaveLinkedinProfileData(final MyProfile myprofileObj)
    {
        String url = "https://api.linkedin.com/v1/people/~:(email-address,first-name,last-name,phone-numbers,location,industry,educations,skills,publicProfileUrl)?format=json";
        APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
        apiHelper.getRequest(this, url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse)
            {
                JSONObject jsonlinkedinData = apiResponse.getResponseDataAsJson();
                LinkedInJSONParser parser = new LinkedInJSONParser(jsonlinkedinData);
                myprofileObj.SaveLinkedinProfile(parser.ParseJSONLinkedinData());
            }
            @Override
            public void onApiError(LIApiError LIApiError) {
             Log.e(Constants.TAG, LIApiError.getMessage());
            }
        });
    }
//	public void ViewRcardClicked(View view)
//	{
//		Intent in = new Intent(this, DisplayrCardActivity.class);
//		startActivity(in);
//	}
//
//	public void SendRcardClicked(View view)
//	{
//		Intent in = new Intent(this, DisplayComapaniesActivity.class);
//		startActivity(in);
//	}

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}

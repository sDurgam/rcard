package sph.durga.rCard.jobseeker;

import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;
import android.widget.Toast;

import sph.durga.rCard.R;

/**
 * Created by durga on 3/30/15.
 */
public class SendViaNFCActivity extends FragmentActivity implements NfcAdapter.CreateNdefMessageCallback
{
    NfcAdapter mNfcAdapter;
    TextView nfcTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nfc_layout);
        nfcTxtView = (TextView) findViewById(R.id.nfctxt);
        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if(mNfcAdapter == null)
        {
            Toast.makeText(this, "NFC is not available", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
        mNfcAdapter.setNdefPushMessageCallback(this, this);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction()))
        {
            processIntent(getIntent());
        }
    }

    void processIntent(Intent intent)
    {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        nfcTxtView.setText(new String(msg.getRecords()[0].getPayload()));
    }


    @Override
    public NdefMessage createNdefMessage(NfcEvent event)
    {
        String text = ("Beam me up, Android!\n\n" + "Beam Time: " + System.currentTimeMillis());
        NdefMessage msg = new NdefMessage(new NdefRecord[] {NdefRecord.createMime("application/vnd.sph.durga.rCard.jobseeker", text.getBytes())});
        return msg;
    }
}

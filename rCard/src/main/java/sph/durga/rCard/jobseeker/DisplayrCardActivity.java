//package sph.durga.rCard.jobseeker;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.support.v4.app.NavUtils;
//import android.support.v7.widget.Toolbar;
//import android.view.MenuItem;
//import android.view.View;
//import android.widget.TextView;
//import android.widget.TextView;
//import android.widget.Spinner;
//import android.widget.TextView;
//import android.widget.ViewSwitcher;
//
//import sph.durga.rCard.BaseActivity;
//import sph.durga.rCard.R;
//import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyRcard;
//
///**
// * Created by durga on 4/7/15.
// */
//public class DisplayrCardActivity extends BaseActivity
//{
//    MyRcard rcardObj;
//    TextView emailTxt;
//    TextView nameTxt;
//    TextView phoneTxt;
//    TextView primaryskillsTxt;
//    TextView androidexpList;
//    TextView iosexpList;
//    TextView androidportfolioTxt;
//    TextView iosportfolioTxt;
//    TextView othportfolioTxt;
//    TextView linkedinTxt;
//    TextView resumeTxt;
//    TextView degreeTxt;
//    TextView otherinfoTxt;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState)
//    {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.jobseeker_display_rcard_activity);
//        emailTxt = (TextView) findViewById(R.id.vemailTxt);
//        nameTxt = (TextView) findViewById(R.id.vnameTxt);
//        phoneTxt = (TextView) findViewById(R.id.vphoneTxt);
//        primaryskillsTxt = (TextView) findViewById(R.id.vprimaryskillsTxt);
//        androidexpList = (TextView) findViewById(R.id.vandroidexpList);
//        iosexpList = (TextView) findViewById(R.id.viosexpList);
//        androidportfolioTxt = (TextView) findViewById(R.id.vandroidportfolioTxt);
//        iosportfolioTxt = (TextView) findViewById(R.id.viosportfolioTxt);
//        othportfolioTxt = (TextView) findViewById(R.id.vothportfolioTxt);
//        linkedinTxt = (TextView) findViewById(R.id.vlinkedinTxt);
//        resumeTxt = (TextView) findViewById(R.id.vresumeTxt);
//        degreeTxt = (TextView) findViewById(R.id.vhighestdegreeTxt);
//        otherinfoTxt = (TextView) findViewById(R.id.votherinfoTxt);
//        toolbar = (Toolbar) findViewById(R.id.customtoolbar);
//        SetUpToolBar();
//    }
//
//    @Override
//    protected void onResume()
//    {
//        super.onResume();
//        rcardObj = new MyRcard(dbHelper);
//        rcardObj.FetchRCard();
//        populateRCard();
//    }
//
//    public void populateRCard()
//    {
//        emailTxt.setText(rcardObj.getEmail() == null ? "" : rcardObj.getEmail());
//        nameTxt.setText(rcardObj.getName() == null ? "" : rcardObj.getName());
//        phoneTxt.setText(rcardObj.getPhone() == null ? "" : rcardObj.getPhone());
//        primaryskillsTxt.setText(rcardObj.getPrimaryskills() == null ? "" : rcardObj.getPrimaryskills());
//        androidexpList.setText(rcardObj.getAndroidexp() == null ? "0" : rcardObj.getAndroidexp().toString());
//        iosexpList.setText(rcardObj.getIosexp() == null ? "0" : rcardObj.getIosexp().toString());
//        androidportfolioTxt.setText(rcardObj.getAndurl() == null ? "" : rcardObj.getAndurl());
//        iosportfolioTxt.setText(rcardObj.getIosurl() == null ? "" : rcardObj.getIosurl());
//        othportfolioTxt.setText(rcardObj.getOthurl() == null ? "" : rcardObj.getOthurl());
//        linkedinTxt.setText(rcardObj.getLinkurl() == null ? "" : rcardObj.getLinkurl());
//        resumeTxt.setText(rcardObj.getResumeurl() == null ? "" : rcardObj.getResumeurl());
//        otherinfoTxt.setText(rcardObj.getOtherinfo() == null ? "" : rcardObj.getOtherinfo());
//        degreeTxt.setText(rcardObj.getDegree() == null ? "" : rcardObj.getDegree());
//    }
//
//    public void EditButtonClicked(View view)
//    {
//        Intent in = new Intent(this, EditrCardActivity.class);
//        startActivity(in);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item)
//    {
//        boolean value = super.onOptionsItemSelected(item);
//        return value;
//    }
//}

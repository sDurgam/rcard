package sph.durga.rCard.jobseeker.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Hashtable;

import sph.durga.rCard.BaseActivity;
import sph.durga.rCard.R;
import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.MyProfile;
import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Skill;
import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;
import sph.durga.rCard.jobseeker.Adapters.SkillsGridAdapter;

/**
* Created by durga on 4/15/15.
*/
public class DisplayProfileActivity extends BaseActivity
{
    MyProfile myprofleObj;

    //card 1
    TextView emailTxt;
    TextView nameTxt;
    TextView phoneTxt;
    TextView countryTxt;
    TextView locationnameTxt;
    //card 2
    TextView industryTxt;
    TextView totalexperienceTxt;
    TextView currentPositionTxt;
    TextView highestDegreeTxt;
    TextView schoolTxt;
    //card 3
    GridView skillsGrid;
    //card 4
    ListView portfoliosGrid;
    //card 5
    TextView linkedinurlTxt;
    TextView resumeurlTxt;
    TextView otherinfoTxt;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_display_activity);
        //cardview 1
        emailTxt = (TextView) findViewById(R.id.profile_emailTxt);
        nameTxt = (TextView) findViewById(R.id.profile_nameTxt);
        phoneTxt = (TextView) findViewById(R.id.profile_phoneTxt);
        countryTxt = (TextView) findViewById(R.id.profile_countryTxt);
        locationnameTxt = (TextView) findViewById(R.id.profile_locationTxt);

        //cardview 2
        industryTxt = (TextView) findViewById(R.id.profile_industryTxt);
        totalexperienceTxt = (TextView) findViewById(R.id.profile_experiencevalueTxt);
        currentPositionTxt = (TextView) findViewById(R.id.profile_currentPositionTxt);
        highestDegreeTxt = (TextView) findViewById(R.id.profile_highestdegreeTxt);
        schoolTxt = (TextView) findViewById(R.id.profile_schoolTxt);

        //cardview 3
        skillsGrid = (GridView) findViewById(R.id.profile_skillsGrid);

        //cardview 4
        portfoliosGrid = (ListView) findViewById(R.id.profile_portfolioGrid);
        //cardview 5
        linkedinurlTxt = (TextView) findViewById(R.id.profile_linkedinTxt);
        resumeurlTxt = (TextView) findViewById(R.id.profile_resumeTxt);
        otherinfoTxt = (TextView) findViewById(R.id.profile_otherinfoTxt);
        toolbar = (Toolbar) findViewById(R.id.profile_customtoolbar);
        SetUpToolBar();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        myprofleObj = new MyProfile(dbHelper);
        myprofleObj.FetchMyProfile();
        DisplayProfile();
    }

    public void DisplayProfile()
    {
        Hashtable<String, Object> profileMap = myprofleObj.getProfileFieldValuesMap();
        //cardview 1
        emailTxt.setText(profileMap.get(SQLiteDBHelper.EMAIL) == null ? "" : profileMap.get(SQLiteDBHelper.EMAIL).toString());
        nameTxt.setText(profileMap.get(SQLiteDBHelper.NAME) == null ? "" : profileMap.get(SQLiteDBHelper.NAME).toString());
        phoneTxt.setText(profileMap.get(SQLiteDBHelper.PHONE) == null ? "" : profileMap.get(SQLiteDBHelper.PHONE).toString());
        countryTxt.setText(profileMap.get(SQLiteDBHelper.COUNTRY) == null ? "" : profileMap.get(SQLiteDBHelper.COUNTRY).toString());
        locationnameTxt.setText(profileMap.get(SQLiteDBHelper.LOCATION_NAME) == null ? "" : profileMap.get(SQLiteDBHelper.LOCATION_NAME).toString());

        //cardview 2
        industryTxt.setText(profileMap.get(SQLiteDBHelper.INDUSTRY) == null ? "" : profileMap.get(SQLiteDBHelper.INDUSTRY).toString());
        totalexperienceTxt.setText(profileMap.get(SQLiteDBHelper.TOTAL_EXP_YEARS_VAL) == null ? "" : profileMap.get(SQLiteDBHelper.TOTAL_EXP_YEARS_VAL).toString());
        currentPositionTxt.setText(profileMap.get(SQLiteDBHelper.CUR_POSITION) == null ? "" : profileMap.get(SQLiteDBHelper.CUR_POSITION).toString());
        highestDegreeTxt.setText(profileMap.get(SQLiteDBHelper.HIGHEST_DEGREE) == null ? "" : profileMap.get(SQLiteDBHelper.HIGHEST_DEGREE).toString());
        schoolTxt.setText(profileMap.get(SQLiteDBHelper.SCHOOL) == null ? "" : profileMap.get(SQLiteDBHelper.SCHOOL).toString());

        //cardview 3
     //   skillsGrid = (GridView) findViewById(R.id.profile_skillsGrid);
          PopulateSkillsGrid((ArrayList<Skill>)profileMap.get(SQLiteDBHelper.SKILLS));

        //cardview 4
     //   portfoliosGrid = (GridView) findViewById(R.id.profile_portfolioGrid);
        //cardview 5
        linkedinurlTxt.setText(profileMap.get(SQLiteDBHelper.LINKEDIN_URL) == null ? "" : profileMap.get(SQLiteDBHelper.LINKEDIN_URL).toString());
        resumeurlTxt.setText(profileMap.get(SQLiteDBHelper.RESUME_URL) == null ? "" : profileMap.get(SQLiteDBHelper.RESUME_URL).toString());
        otherinfoTxt.setText(profileMap.get(SQLiteDBHelper.OTHER_INFO) == null ? "" : profileMap.get(SQLiteDBHelper.OTHER_INFO).toString());
    }

    public void EditButtonClicked(View view)
    {
        Intent in = new Intent(this, EditProfileActivity.class);
        startActivity(in);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        boolean value = super.onOptionsItemSelected(item);
        return value;
    }

    private void PopulateSkillsGrid(ArrayList<Skill> skillsList)
    {
        SkillsGridAdapter  adapter = new SkillsGridAdapter(this, skillsList);
        skillsGrid.setAdapter(adapter);
    }
}

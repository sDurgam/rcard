package sph.durga.rCard.jobseeker;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Hashtable;

import sph.durga.rCard.Constants;
import sph.durga.rCard.db.SQLiteDBHelper.ORClasses.jobseeker.Skill;
import sph.durga.rCard.db.SQLiteDBHelper.SQLiteDBHelper;

/**
 * Created by durga on 4/16/15.
 */
public class LinkedInJSONParser
{
    JSONObject data;

    public LinkedInJSONParser(JSONObject data)
    {
        this.data = data;
    }

    protected Hashtable<String, Object> ParseJSONLinkedinData()
    {
        Hashtable<String, Object> profileData = new Hashtable<String,Object>();
        ArrayList<Skill> skillsList = new ArrayList<Skill>();
        JSONObject schoolObj = null;
        String country = "";
        String locationname = "";
        String degree = "";
        String school = "";
        String phoneNumber = "";

        try
        {
            JSONObject educations = (JSONObject)data.get("educations");
            JSONObject locationObj = ((JSONObject)data.get("location"));
            if(locationObj != null)
            {
                country = locationObj.getString("country");
                locationname = locationObj.getString("name");
            }
            if(educations != null && educations.length() > 0 && ((JSONArray) ((JSONObject)data.get("educations")).get("values")).length() > 0)
            {
                schoolObj = (JSONObject)((JSONArray)(((JSONObject)data.get("educations")).get("values"))).get(0);
            }
            if(schoolObj != null)
            {
                degree = schoolObj.getString("degree");
                school = schoolObj.getString("schoolName");
            }
            profileData.put(SQLiteDBHelper.EMAIL, data.get("emailAddress"));
            profileData.put(SQLiteDBHelper.NAME, data.get("firstName") + " " + data.get("lastName"));
            profileData.put(SQLiteDBHelper.PHONE, "");
            profileData.put(SQLiteDBHelper.COUNTRY, country);
            profileData.put(SQLiteDBHelper.LOCATION_NAME, locationname);
            profileData.put(SQLiteDBHelper.INDUSTRY, data.get("industry"));
            profileData.put(SQLiteDBHelper.CUR_POSITION, "");
            profileData.put(SQLiteDBHelper.TOTAL_EXP_YEARS_VAL, "");
            profileData.put(SQLiteDBHelper.HIGHEST_DEGREE, degree);
            profileData.put(SQLiteDBHelper.SCHOOL, school);
            profileData.put(SQLiteDBHelper.SKILLS, ParseSkills((JSONObject)data.get("skills")));
            profileData.put(SQLiteDBHelper.PORTFOLIOS, "");
            profileData.put(SQLiteDBHelper.LINKEDIN_URL, data.get("publicProfileUrl"));
            profileData.put(SQLiteDBHelper.RESUME_URL, "");
            profileData.put(SQLiteDBHelper.OTHER_INFO, "");
        }
        catch (JSONException jex)
        {
            Log.e(Constants.TAG, jex.getMessage());
        }
        return profileData;
    }
    private ArrayList<Skill> ParseSkills(JSONObject skillsObj)  throws JSONException {
        ArrayList<Skill> skillsList = new ArrayList<Skill>();
        if (skillsObj != null) {
            int total = skillsObj.getInt("_total");
            if (total > 0) {
                JSONArray skillValuesArray = skillsObj.getJSONArray("values");
                String name = "";
                for (int i = 0; i < skillValuesArray.length(); i++) {
                    name = ((JSONObject) ((JSONObject) skillValuesArray.get(i)).get("skill")).getString("name");
                    Skill skill = new Skill(i + 1, name, "0");
                    skillsList.add(skill);
                }
            }
        }
        return skillsList;
    }
}
